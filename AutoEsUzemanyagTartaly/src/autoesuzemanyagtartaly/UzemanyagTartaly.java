/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package autoesuzemanyagtartaly;


import com.sun.org.apache.xml.internal.resolver.helpers.PublicId;
/**
 *
 * @author sz6karatt
 */
public class UzemanyagTartaly {
    //konstans változók megadása
    private double terfogat;
    private double mennyiseg;
    
    //konstruktor létrehozása
    public UzemanyagTartaly(double terfogat){
        if(terfogat>=40)
        {
            this.terfogat=terfogat;
            mennyiseg=terfogat;
        }
        else
        {
            throw new ArithmeticException("40 liternél kisebb tartály nem hozható létre!");
        }
        
    }
    public UzemanyagTartaly(double terfogat,double mennyiseg)throws Exception{
        if(mennyiseg>0 && terfogat>=40)
        {
            this.terfogat=terfogat;
            this.mennyiseg=mennyiseg;
        }
        else
        {
            throw new ArithmeticException("");
        }
    }
    public double getMennyiseg()
    {
        return mennyiseg;
    }

    public double getTerfogat() {
        return terfogat;
    }
    public String getMennyisegSzazalekban(){
        
        return terfogat%100*mennyiseg+"% uzemanyag van";
    }
    public boolean toltes(double menyiseg){
        if(menyiseg>0){
            if(this.mennyiseg+menyiseg>this.terfogat){
                this.mennyiseg=terfogat;
            }else{
                this.mennyiseg+=menyiseg;
            }            
            return true;
        }else{
            return false;
        }
    }
    public boolean elegetes(double liter){
        if(liter>this.mennyiseg){
            return false;
        }else{
            this.mennyiseg-=liter;
            return true;
        }
    }

    @Override
    public String toString() {
        return "maximális méret: "+this.terfogat+"\ntárolt üzemanyag: "+this.mennyiseg+"\nszázalékos foglaltsá: "+getTerfogat();
    }
    
}
